const root = document.getElementById('root')


const books = [
  {
    author: "Люсі Фолі",
    name: "Список запрошених",
    price: 70
  },
  {
    author: "Сюзанна Кларк",
    name: "Джонатан Стрейндж і м-р Норрелл",
  },
  {
    name: "Дизайн. Книга для недизайнерів.",
    price: 70
  },
  {
    author: "Алан Мур",
    name: "Неономікон",
    price: 70
  },
  {
    author: "Террі Пратчетт",
    name: "Рухомі картинки",
    price: 40
  },
  {
    author: "Анґус Гайленд",
    name: "Коти в мистецтві",
  }
];

const ul = document.createElement('ul');

for (let i = 0; i < books.length; i++) {
  const li = document.createElement('li');
  const book = books[i];
  try {
    let text = "";
    if (book.author) {
      text += ` ${book.author}`;
    } else {
      throw new Error("absent author")
    }
    if (book.name) {
      text += ` - ${book.name}`;
    } else {
      throw new Error("absent name")
    }
    if (book.price) {
      text += ` - ${book.price} грн`;
    } else {
      throw new Error("absent price")
    }
    li.textContent = text;
    ul.appendChild(li);
  } catch (err) {
    console.log(err.message);
  }
}
root.appendChild(ul);















